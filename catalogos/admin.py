from django.contrib import admin
from .models import Pais, Departamento


class PaisAdmin(admin.ModelAdmin):
    list_display = ['nombre']


class DepartamentoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'pais', 'active']
    list_filter = ['pais__nombre', 'active']
    search_fields = ['nombre', 'pais__nombre']


admin.site.register(Departamento, DepartamentoAdmin)
admin.site.register(Pais, PaisAdmin)
